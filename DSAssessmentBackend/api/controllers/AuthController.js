/**
 * AuthController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const bcrypt = require('bcrypt-nodejs');

module.exports.login = function (req, res) {
  var user = User.findOne({
    email: req.body.email
  }).then(function (user) {
    if (!user) {
      return res.json({
        err: 300
      });
    } else {
      if (!bcrypt.compareSync(req.body.password, user.password)) {
        return res.json({
          err: 300
        });
      } else {
        delete user.password;
        return res.json(user);
      }
    }
  }).catch(function (err) {
    sails.log(err);
    return res.json({
      err: 203
    })
  })
};

module.exports.register = function (req, res) {
  var user = req.body;
  if (user.password.length < 5) {
    return res.json({
      err: 202
    });
  } else {
    var takenMail = User.findOne({
      email: user.email
    }).then(function (takenMail) {
      if (takenMail) {
        return res.json({
          err: 200
        });
      } else {
        var takenPhone = User.findOne({

          phone: user.phone

        }).then(function (takenPhone) {
          if (takenPhone) {
            return res.json({
              err: 201
            });
          } else {
            user.password = bcrypt.hashSync(user.password, bcrypt.genSaltSync(12), null);
            User.create(user).then(function (newUser) {
              delete user.password;
              return res.json(user);
            }).catch(function (err3) {
              sails.log(err3);
              return res.json({
                err: 203
              })
            })

          }
        }).catch(function (err1) {
          sails.log(err1);
          return res.json({
            err: 203
          })
        })
      }
    }).catch(function (err2) {
      sails.log(err2);
      return res.json({
        err: 203
      })
    })
  }
};
