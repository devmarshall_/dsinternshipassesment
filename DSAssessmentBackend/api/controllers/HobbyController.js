/**
 * HobbyController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */



var client = require('twilio')(
  sails.config.custom.TWILIO_ACCOUNT_SID,
  sails.config.custom.TWILIO_AUTH_TOKEN
);

var mailgun = require('mailgun-js')({
  apiKey: sails.config.custom.MAILGUN_API_KEY,
  domain: sails.config.custom.MAILGUN_DOMAIN
});


module.exports.newHobby = function (req, res) {
  var hobby = {
    user_id: req.body.user_id,
    name: req.body.name
  }

  User.find({
    _id: hobby.user_id
  }).then(function (user) {

    Hobby.create(hobby).then(function (newHobby) {

      client.messages.create({
        body: "You just sent an added " + hobby.name + " as a hobby",
        from: sails.config.custom.TWILIO_PHONE_NUMBER,
        to: user[0].phone
      }).then((message) => console.log(message.sid)).done();


      var mailData = {
        from: 'DS INTERNSHIP ASSESSMENT <me@samples.mailgun.org>',
        to: user.email,
        subject: 'New Hobby',
        text: "You just sent an added " + hobby.name + " as a hobby"
      };

      mailgun.messages().send(mailData, function (error, body) {
        sails.log(body);
      });

      return res.json({
        msg: 'Hobby created'
      })

    }).catch(function (err) {
      sails.log(err);
      res.json({
        err: 203
      })
    })
  }).catch(function (err) {
    sails.log(err);
    return res.json({
      err: 203
    })
  })



}

module.exports.getHobbies = function (req, res) {
  Hobby.find({
    where: {
      user_id: req.body.id
    }
  }).then(function (Hobbies) {

    for (var i = 0; i < Hobbies.length; i++) {
      Hobbies[i] = Hobbies[i].name
    }

    res.json({
      hobbies: Hobbies
    });
  }).catch(function (err) {
    sails.log(err);
    res.json({
      err: 203
    });
  })


}
