/**
 * Hobby.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    name: {
      type: 'string',
      required: true,
      columnName: 'name'
    },
    user_id: {
      type: 'string',
      required: true,
      columnName: 'user_id'
    }
  }

};
