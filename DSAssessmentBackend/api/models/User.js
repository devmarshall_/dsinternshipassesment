/**
 * User.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */



module.exports = {
  attributes: {
    f_name: {
      type: 'string',
      required: true,
      columnName: 'f_name'
    },

    l_name: {
      type: 'string',
      required: true,
      columnName: 'l_name'
    },

    phone: {
      type: 'string',
      required: true,
      unique: true,
      columnName: 'phone_number'
    },

    email: {
      type: 'string',
      required: true,
      unique: true,
      columnName: 'email'
    },

    password: {
      type: 'string',
      columnName: 'password',
      required: true,
    }
  }
}
