import React, { Component } from "react";
import globalStorage from "react-global-storage";
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";

import "./App.css";

import Login from "./components/Login";
import Home from "./components/Home";
import SignUp from "./components/SignUp";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoggedIn: false
    };
    this.loggedIn = this.loggedIn.bind(this);
    this.logout = this.logout.bind(this);
  }

  componentWillMount() {
    if (this.loggedIn()) {
      this.setState({
        isLoggedIn: true
      });
      this.setState({
        user: JSON.parse(window.localStorage["User-Data"])
      });
    } else {
      this.setState({
        isLoggedIn: false
      });
    }
  }

  loggedIn() {
    if (localStorage.getItem("User-Data")) {
      return true;
    } else {
      return false;
    }
  }

  logout() {
    window.localStorage.clear();
    this.setState({
      isLoggedIn: false
    });
    globalStorage.setState("isLoggedIn", false, this);
    window.location = "/guest/login";
  }

  render() {
    return (
      <div>
        <Router>
          <div>
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
              <div className="container">
                <Link className="navbar-brand" to="/">
                  DS Internship Assesment
                </Link>
                <button
                  className="navbar-toggler"
                  type="button"
                  data-toggle="collapse"
                  data-target="#navbarNav"
                  aria-controls="navbarNav"
                  aria-expanded="false"
                  aria-label="Toggle navigation"
                >
                  <span className="navbar-toggler-icon" />
                </button>
                <div className="collapse navbar-collapse" id="navbarNav">
                  <ul className="navbar-nav">
                    {this.state.isLoggedIn ? (
                      <div>
                        <li className="nav-item">
                          <a className="nav-link" onClick={this.logout}>
                            Logout
                          </a>
                        </li>
                      </div>
                    ) : (
                      <div>
                        <li className="nav-item">
                          <Link className="nav-link" to="/guest/login">
                            Login <span className="sr-only">(current)</span>
                          </Link>
                        </li>
                        <li className="nav-item">
                          <Link className="nav-link" to="/guest/signup">
                            SignUp
                          </Link>
                        </li>
                      </div>
                    )}
                  </ul>
                </div>
              </div>
            </nav>
            <div className="App">
              <div className="container">
                <Switch>
                  <Route exact={true} path="/guest/login" component={Login} />
                  <Route exact={true} path="/guest/signup" component={SignUp} />
                  <Route exact={true} path="/" component={Home} />
                  <Route path="*" component={Home} />
                </Switch>
              </div>
            </div>
          </div>
        </Router>
      </div>
    );
  }
}

export default App;
