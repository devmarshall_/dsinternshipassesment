import React from "react";

import globalStorage from "react-global-storage";
import "../App.css";

class Alert extends React.Component {
  componentDidMount() {
    document.getElementById("login-alert").innerHTML =
      globalStorage.state.loginErr;
  }

  render() {
    return <div id="login-alert" className="alert alert-danger" role="alert" />;
  }
}

export default Alert;
