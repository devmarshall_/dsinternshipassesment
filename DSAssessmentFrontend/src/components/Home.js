import React from "react";
import axios from "axios";

import { apiUrl } from "../config";

class Home extends React.Component {
  constructor(props, context) {
    super(props);
    this.state = {
      isLoggedIn: false,
      newHobby: "",
      Hobbies: [],
    };

    this.onHobbyInputChange = this.onHobbyInputChange.bind(this);
    this.loggedIn = this.loggedIn.bind(this);
    this.createNewHobby = this.createNewHobby.bind(this);
    this.getHobbies = this.getHobbies.bind(this);
  }

  onHobbyInputChange(e) {
    this.setState({ newHobby: e.target.value });
  }

  createNewHobby() {
    var self = this;
    axios
      .post(apiUrl + "/hobby/new", {
        user_id: self.state.user.id,
        name: self.state.newHobby
      })
      .then(function(response) {
        if (!response.data.err) {
          self.state.Hobbies.push(self.state.newHobby);
          self.setState({
            newHobby: ""
          });
        }
      })
      .catch(function(err) {
        console.log(err);
      });
  }
  loggedIn() {
    if (localStorage.getItem("User-Data")) {
      return true;
    } else {
      return false;
    }
  }

  async componentWillMount() {
    if (this.loggedIn()) {
      this.state.isLoggedIn = true;
      this.state.user = JSON.parse(window.localStorage["User-Data"]);
      this.getHobbies();
    } else {
      window.location = "/guest/login";
    }
  }

  getHobbies() {
    var payload = {
      id: JSON.parse(window.localStorage["User-Data"])._id
    };
    var self = this;
    axios
      .post(apiUrl + "/hobbies", payload)
      .then(function(response) {
        self.state.Hobbies = response.data.hobbies;
        self.forceUpdate();
      })
      .catch(function(err) {
        console.log(err);
      });
  }
  render() {
    return (
      <div>
        <div className="container">
          <div className="row">
            <div className="col-sm-4">
              <h3>
                Hello, {this.state.user.f_name} {this.state.user.l_name}
              </h3>
              <br />

              <button
                type="button"
                className="btn btn-primary"
                data-toggle="modal"
                data-target="#newHobbyModal"
              >
                Add a Hobby
              </button>
            </div>
            <div className="col-sm-6">
              <div className="container">
                <div className="card text-center">
                  <div className="card-header">My Hobbies</div>
                  <ul className="list-group list-group-flush">
                    {this.state.Hobbies.map(hobby => {
                      return <li className="list-group-item">{hobby}</li>;
                    })}
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div
          className="modal fade"
          id="newHobbyModal"
          tabIndex="-1"
          role="dialog"
          aria-labelledby="exampleModalCenterTitle"
          aria-hidden="true"
        >
          <div className="modal-dialog modal-dialog-centered" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalCenterTitle">
                  New Hobby
                </h5>
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="modal-body">
                <form>
                  <label htmlFor="inputHobby" className="sr-only">
                    Email address
                  </label>

                  <input
                    type="text"
                    id="inputHobby"
                    onChange={this.onHobbyInputChange}
                    className="form-control"
                    placeholder="Hobby Name"
                    required
                  />
                </form>
              </div>
              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-secondary"
                  data-dismiss="modal"
                >
                  Close
                </button>
                <button
                  type="submit"
                  className="btn btn-primary"
                  onClick={this.createNewHobby}
                  data-dismiss="modal"
                >
                  Create
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Home;
