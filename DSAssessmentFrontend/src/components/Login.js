import React from "react";
import globalStorage from "react-global-storage";
import axios from "axios";

import "../App.css";
import Alert from "./Alert";

import { apiUrl } from "../config";

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      err: false,
      isLoggedIn: false,
      email: "",
      password: ""
    };

    this.onEmailChange = this.onEmailChange.bind(this);
    this.onPasswordChange = this.onPasswordChange.bind(this);

    this.checkLoggedIn = this.checkLoggedIn.bind(this);
    this.login = this.login.bind(this);
    this.validateEmail = this.validateEmail.bind(this);
  }

  onEmailChange(e) {
    this.setState({ email: e.target.value });
  }
  onPasswordChange(e) {
    this.setState({ password: e.target.value });
  }

  login(e) {
    e.preventDefault();
    this.setState({ err: false });
    globalStorage.setState("loginErr", null, this);
    if (
      this.state.password === "" ||
      this.state.email === "" ||
      !this.validateEmail(this.state.email)
    ) {
      globalStorage.setState(
        "loginErr",
        "All fields must be filled out and Email must be valid!",
        this
      );
      this.setState({ err: true });
    } else {
      globalStorage.setState("loginErr", null, this);
      this.setState({ err: false });

      var user = {
        email: this.state.email,
        password: this.state.password
      };
      var self = this;
      axios
        .post(apiUrl + "/login", user)
        .then(function(response) {
          console.log(response);
          if (!response.data.err) {
            localStorage.setItem("User-Data", JSON.stringify(response.data));
            globalStorage.setState("isLoggedIn", true);
            self.props.history.push("/");
            window.location.reload();
          } else if (response.data.err === 203) {
            globalStorage.setState(
              "loginErr",
              "Server currently down, please try again",
              this
            );
            self.setState({ err: true });
          } else if (response.data.err === 300) {
            globalStorage.setState(
              "loginErr",
              "Sorry, doesn't match any of our records",
              this
            );
            self.setState({ err: true });
          }
        })
        .catch(function(err) {
          console.log(err);
        });
    }
  }

  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  componentDidMount() {
    this.checkLoggedIn();
  }

  checkLoggedIn() {
    if (!localStorage.getItem("User-Data")) {
      this.setState({
        isLoggedIn: false
      });
      globalStorage.setState("isLoggedIn", false, this);
    } else {
      globalStorage.setState("isLoggedIn", true, this);
      this.props.history.push("/");
    }
  }
  render() {
    return (
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-sm-5">
            <form className="form-signin">
              <h1 className="h3 mb-3 font-weight-normal">Please sign in</h1>

              {this.state.err && <Alert />}
              <label htmlFor="inputEmail" className="sr-only">
                Email address
              </label>
              <input
                type="email"
                id="inputEmail"
                className="form-control"
                onChange={this.onEmailChange}
                placeholder="Email address"
                required
                autoFocus
              />
              <label htmlFor="inputPassword" className="sr-only">
                Password
              </label>
              <input
                type="password"
                id="inputPassword"
                onChange={this.onPasswordChange}
                className="form-control"
                placeholder="Password"
                required
              />
              <br />
              <button
                className="btn btn-lg btn-primary btn-block"
                onClick={this.login}
                type="submit"
              >
                Log in
              </button>
            </form>
          </div>
        </div>
        <div className="container" />
      </div>
    );
  }
}

export default Login;
