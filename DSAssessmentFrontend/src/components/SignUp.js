import React from "react";
import axios from "axios";
import globalStorage from "react-global-storage";

import "../App.css";
import Alert from "./Alert";

import { apiUrl } from "../config";

class SignUp extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      err: false,
      f_name: "",
      l_name: "",
      phone: "",
      email: "",
      password: "",
      password_confirm: ""
    };

    this.checkLoggedIn = this.checkLoggedIn.bind(this);

    this.onF_NameChange = this.onF_NameChange.bind(this);
    this.onL_NameChange = this.onL_NameChange.bind(this);
    this.onEmailChange = this.onEmailChange.bind(this);
    this.onPhoneChange = this.onPhoneChange.bind(this);
    this.onPasswordChange = this.onPasswordChange.bind(this);
    this.onPassword_ConfirmChange = this.onPassword_ConfirmChange.bind(this);

    this.register = this.register.bind(this);
    this.checkLoggedIn = this.checkLoggedIn.bind(this);
    this.validateEmail = this.validateEmail.bind(this);
  }

  onF_NameChange(e) {
    this.setState({ f_name: e.target.value });
  }
  onL_NameChange(e) {
    this.setState({ l_name: e.target.value });
  }
  onPhoneChange(e) {
    this.setState({ phone: e.target.value });
  }
  onEmailChange(e) {
    this.setState({ email: e.target.value });
  }
  onPasswordChange(e) {
    this.setState({ password: e.target.value });
  }
  onPassword_ConfirmChange(e) {
    this.setState({ password_confirm: e.target.value });
  }
  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  checkLoggedIn() {
    if (!localStorage.getItem("User-Data")) {
      this.setState({
        isLoggedIn: false
      });
      globalStorage.setState("isLoggedIn", false, this);
    } else {
      globalStorage.setState("isLoggedIn", true, this);
      this.props.history.push("/");
    }
  }

  register(e) {
    e.preventDefault();

    if (
      !this.state.password === this.state.password_confirm ||
      this.state.f_name === "" ||
      this.state.l_name === "" ||
      this.state.email === "" ||
      this.state.phone === "" ||
      this.state.password === "" ||
      this.state.password_confirm === "" ||
      this.state.password.length < 5
    ) {
      globalStorage.setState(
        "loginErr",
        "All fields must be filled out, email must be valid and password must be longer than 5 characters!",
        this
      );
      this.setState({ err: true });
    } else {
      globalStorage.setState("loginErr", null, this);
      this.setState({ err: false });
      var newUser = this.state;
      delete newUser.err;
      delete newUser.password_confirm;
      var self = this;
      axios
        .post(apiUrl + "/register", newUser)
        .then(function(response) {
          console.log(response);
          if (!response.data.err) {
            localStorage.setItem("User-Data", JSON.stringify(response.data));
            globalStorage.setState("isLoggedIn", true);
            self.props.history.push("/");
          } else if (response.data.err === 200) {
            globalStorage.setState(
              "loginErr",
              "Sorry, email already taken",
              this
            );
            this.setState({ err: true });
          } else if (response.data.err === 201) {
            globalStorage.setState(
              "loginErr",
              "Sorry, phone number already taken",
              this
            );
            this.setState({ err: true });
          } else if (response.data.err === 202) {
            globalStorage.setState(
              "loginErr",
              "Password length is too short",
              this
            );
            this.setState({ err: true });
          } else if (response.data.err === 203) {
            globalStorage.setState(
              "loginErr",
              "Server currently down, plases try again",
              this
            );
            this.setState({ err: true });
          }
        })
        .catch(function(err) {
          console.log(err);
        });
    }
  }

  render() {
    return (
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-sm-5">
            <form>
              <h1 className="h3 mb-3 font-weight-normal">
                Why don't you join up?
              </h1>
              {this.state.err && <Alert />}
              <div className="form-group">
                <label htmlFor="f_name" className="sr-only">
                  First Name
                </label>
                <input
                  type="text"
                  id="f_name"
                  className="form-control"
                  onChange={this.onF_NameChange}
                  placeholder="First Name"
                  required
                  autoFocus
                />
              </div>

              <div className="form-group">
                <label htmlFor="l_name" className="sr-only">
                  Last Name
                </label>
                <input
                  type="text"
                  id="l_name"
                  onChange={this.onL_NameChange}
                  className="form-control"
                  placeholder="Last Name"
                  required
                  autoFocus
                />
              </div>

              <div className="form-group">
                <label htmlFor="email" className="sr-only">
                  Email address
                </label>
                <input
                  type="email"
                  id="email"
                  className="form-control"
                  onChange={this.onEmailChange}
                  placeholder="Email address"
                  required
                  autoFocus
                />
              </div>

              <div className="form-group">
                <label htmlFor="phone" className="sr-only">
                  Phone Number
                </label>
                <input
                  type="tel"
                  id="phone"
                  className="form-control"
                  onChange={this.onPhoneChange}
                  placeholder="Phone Number"
                  required
                  autoFocus
                />
              </div>

              <div className="form-group">
                <label htmlFor="password" className="sr-only">
                  Password
                </label>
                <input
                  type="password"
                  id="password"
                  onChange={this.onPasswordChange}
                  className="form-control"
                  placeholder="Password"
                  required
                />
                <input
                  type="password"
                  id="password_confirm"
                  className="form-control"
                  onChange={this.onPassword_ConfirmChange}
                  placeholder="Confirm Password"
                  required
                />
              </div>
              <br />
              <button
                className="btn btn-lg btn-primary btn-block"
                onClick={this.register}
                type="submit"
              >
                Register
              </button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default SignUp;
